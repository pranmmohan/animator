import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.AbstractShape;
import cs3500.animator.model.Animator;
import cs3500.animator.model.Model;
import cs3500.animator.view.SVGView;
import cs3500.animator.utils.TweenModelBuilder;
import cs3500.animator.view.View;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import org.junit.Test;

public class SVGViewTest {

  TweenModelBuilder builder;
  View sVG;
  private OutputStream out = new ByteArrayOutputStream();

  TweenModelBuilder builder2;
  View sVG2;
  private OutputStream out2 = new ByteArrayOutputStream();

  @Test
  public void simpleBaseTest() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<rect x=\"10.0\" y=\"10.0\" width=\"30.0\" height =\"20.0\" visibility = \"hidden\"" +
            " fill = \"rgb(51,51,26)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"0.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"30.0\"/>\n" +
            "<animate begin =\"3.0\" attributename =\"x\" from = \"30.0\" to = \"40.0\"" +
            " dur = \"2.0\"/>\n" +
            "<animate begin =\"3.0\" attributename =\"y\" from = \"40.0\" to = \"40.0\"" +
            " dur = \"2.0\"/>\n" +
            "<set attributename = \"x\" from = \"30.0\" to = \"40.0\" begin = \"5.0\" />\n" +
            "<set attributename = \"y\" from = \"40.0\" to = \"40.0\" begin = \"5.0\" />\n" +
            "</rect>\n" +
            "<ellipse cx=\"60.0\" cy=\"10.0\" rx=\"30.0\" ry=\"90.0\" \n" +
            "visibility = \"hidden\"fill = \"rgb(26,77,255)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"10.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"40.0\"/>\n" +
            "</ellipse>\n" +
            "\n" +
            "</svg>\n" +
            "\n";
    builder = new Model.Builder(1);

    builder.addOval("O",60, 10, 30, 90, 0.1f,
        0.3f,1.0f, 10, 40 );
    builder.addRectangle("R", 10, 10, 30, 20, 0.2f,
        0.2f, 0.1f, 0, 30);
    builder.addMove("R", 30, 40, 40, 40,
            3, 5);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output,out.toString());
  }

  /**
   * Test passing in no shapes.
   */
  @Test
  public void noShapes() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "\n" +
            "</svg>\n" +
            "\n";

    builder = new Model.Builder(1);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output,out.toString());
  }

  /**
   *  Check that changing the tick rate results in different appear and disappear time.
   */
  @Test
  public void changeTickRate() {
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addMove("O1",60,60,100,100,17,
            30);

    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    builder2 = new Model.Builder(1);
    builder2.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder2.addMove("O1",60,60,100,100,17,
            30);

    Animator model2 = (Animator)builder2.build();

    List<AbstractShape> shapes2 = model2.getShapes();
    List<AbstractAnimation> animations2 = model2.getAnimations();

    sVG2 = new SVGView();
    sVG2.initialize(shapes2, animations2);
    sVG2.render(500000, 500000, 4, new PrintStream(out2));

    assertNotEquals(out.toString(),out2.toString());
  }

  /**
   * Test passing in empty animation.
   */
  @Test
  public void noAnimations() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<rect x=\"50.0\" y=\"50.0\" width=\"14.0\" height =\"14.0\" visibility = \"hidden\"" +
            " fill = \"rgb(51,51,26)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"5.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"10.0\"/>\n" +
            "</rect>\n" +
            "\n" +
            "</svg>\n" +
            "\n";

    builder = new Model.Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 5, 10);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output,out.toString());
  }

  /**
   * Test for invalid start and end times.
   */
  @Test (expected = IllegalArgumentException.class)
  public void invalidAppearTime() {
    builder = new Model.Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, -5, -1);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));
  }

  /**
   * Passing invalid time for animation times.
   */
  @Test (expected = IllegalArgumentException.class)
  public void invalidAnimationTime() {
    builder = new Model.Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 10, 20);
    builder.addMove("R1",60,60,100,100,-4,
            -1);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));
  }

  /**
   * Specify an animation that is invalid - occurs before the shape appears.
   */
  @Test (expected = IllegalArgumentException.class)
  public void invalidAnimation() {
    builder = new Model.Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 10, 20);
    builder.addMove("R1",60,60,100,100,8,
            13);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();
    System.out.print("Animations: " + animations);

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));
  }

  /**
   * Specify an animation that is invalid - occurs after the shape disappears.
   */
  @Test (expected = IllegalArgumentException.class)
  public void invalidAnimation2() {
    builder = new Model.Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 10, 20);
    builder.addMove("R1",60,60,100,100,25,
            30);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));
  }

  /**
   * Test moving a Rectangle during a move animation that is already occuring.
   */
  @Test (expected = IllegalArgumentException.class)
  public void moveAnimationOverlap() {
    builder = new Model.Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 0, 20);
    builder.addMove("R1",60,60,100,100,3,
            15);
    builder.addMove("R1",10,10,17,30,7,
            9);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));
  }

  /**
   * Test changing color of a Rectangle during an existing color change animation for that
   * Rectangle.
   */
  @Test (expected = IllegalArgumentException.class)
  public void changeColorOverlap() {
    builder = new Model.Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 0, 20);
    builder.addColorChange("R1",0.1f,0.4f,0.9f,0.3f,0.5f,
            0.2f,2,10);
    builder.addColorChange("R1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,5,8);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));
  }

  /**
   * Test changing size of a Oval during an existing size change animation for it.
   */
  @Test (expected = IllegalArgumentException.class)
  public void reSizeOverlap() {
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addScaleToChange("O1",10,17,20,40,12,
            20);
    builder.addScaleToChange("O1",1,10,6,30,15,
            17);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));
  }

  /**
   * Test changing color of a Oval while moving it.
   */
  @Test
  public void changeColorWhileMoving() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<ellipse cx=\"40.0\" cy=\"30.0\" rx=\"5.0\" ry=\"10.0\" \n" +
            "visibility = \"hidden\"fill = \"rgb(77,77,51)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"10.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"50.0\"/>\n" +
            "</ellipse>\n" +
            "\n" +
            "</svg>\n" +
            "\n";
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addMove("O1",60,60,100,100,17,
            30);
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,20,25);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test changing color of a Oval while resizing it.
   */
  @Test
  public void changeColorWhileResizing() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<ellipse cx=\"40.0\" cy=\"30.0\" rx=\"5.0\" ry=\"10.0\" \n" +
            "visibility = \"hidden\"fill = \"rgb(77,77,51)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"10.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"50.0\"/>\n" +
            "</ellipse>\n" +
            "\n" +
            "</svg>\n" +
            "\n";
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addScaleToChange("O1",10,17,20,40,12,
            20);
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,14,17);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test changing size of a Oval while moving it.
   */
  @Test
  public void reSizeWhileMoving() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<ellipse cx=\"40.0\" cy=\"30.0\" rx=\"5.0\" ry=\"10.0\" \n" +
            "visibility = \"hidden\"fill = \"rgb(77,77,51)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"10.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"50.0\"/>\n" +
            "</ellipse>\n" +
            "\n" +
            "</svg>\n" +
            "\n";
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addMove("O1",60,60,100,100,17,
            30);
    builder.addScaleToChange("O1",10,17,20,40,20,
            23);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test resize Oval while changing color.
   */
  @Test
  public void resizeWhileChangeColor() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<ellipse cx=\"40.0\" cy=\"30.0\" rx=\"5.0\" ry=\"10.0\" \n" +
            "visibility = \"hidden\"fill = \"rgb(77,77,51)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"10.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"50.0\"/>\n" +
            "</ellipse>\n" +
            "\n" +
            "</svg>\n" +
            "\n";
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,17,24);
    builder.addScaleToChange("O1",10,17,20,40,20,
            23);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test moving Oval while changing color.
   */
  @Test
  public void moveWhileChangeColor() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<ellipse cx=\"40.0\" cy=\"30.0\" rx=\"5.0\" ry=\"10.0\" \n" +
            "visibility = \"hidden\"fill = \"rgb(77,77,51)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"10.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"50.0\"/>\n" +
            "</ellipse>\n" +
            "\n" +
            "</svg>\n" +
            "\n";
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,17,24);
    builder.addMove("O1",60,60,100,100,20,
            23);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test moving Oval while resizing.
   */
  @Test
  public void moveWhileResizing() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<ellipse cx=\"40.0\" cy=\"30.0\" rx=\"5.0\" ry=\"10.0\" \n" +
            "visibility = \"hidden\"fill = \"rgb(77,77,51)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"10.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"50.0\"/>\n" +
            "</ellipse>\n" +
            "\n" +
            "</svg>\n" +
            "\n";
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addScaleToChange("O1",10,17,20,40,17,
            30);
    builder.addMove("O1",60,60,100,100,20,
            23);

    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test multiple consecutive moves with overlap(of different animations) and multiple shapes.
   */
  @Test
  public void multipleMoves() {
    String output = "<svg width = \"5000\" height = \"5000\">\n" +
            "\n" +
            "<rect x=\"50.0\" y=\"50.0\" width=\"14.0\" height =\"14.0\" visibility = \"hidden\"" +
            " fill = \"rgb(51,51,26)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"0.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"30.0\"/>\n" +
            "<animate begin =\"2.0\" attributename =\"x\" from = \"40.0\" to = \"60.0\"" +
            " dur = \"8.0\"/>\n" +
            "<animate begin =\"2.0\" attributename =\"y\" from = \"30.0\" to = \"60.0\"" +
            " dur = \"8.0\"/>\n" +
            "<set attributename = \"x\" from = \"40.0\" to = \"60.0\" begin = \"10.0\" />\n" +
            "<set attributename = \"y\" from = \"30.0\" to = \"60.0\" begin = \"10.0\" />\n" +
            "<animate begin =\"12.0\" attributename =\"x\" from = \"10.0\" to = \"200.0\"" +
            " dur = \"3.0\"/>\n" +
            "<animate begin =\"12.0\" attributename =\"y\" from = \"20.0\" to = \"100.0\"" +
            " dur = \"3.0\"/>\n" +
            "<set attributename = \"x\" from = \"10.0\" to = \"200.0\" begin = \"15.0\" />\n" +
            "<set attributename = \"y\" from = \"20.0\" to = \"100.0\" begin = \"15.0\" />\n" +
            "<animate begin =\"13.0\" attributename =\"fill\" from = \"rgb(77,179,51)\" " +
            "to = \"rgb(26,77,51)\" dur = \"1.0\"/>\n" +
            "<set attributename = \"fill\" from = \"rgb(77,179,51)\" to = \"rgb(26,77,51)\" " +
            "begin = \"14.0\" />\n" +
            "<animate begin =\"17.0\" attributename =\"width\" from = \"10.0\" to = \"20.0\" " +
            "dur = \"13.0\"/>\n" +
            "<animate begin =\"17.0\" attributename =\"height\" from = \"17.0\" to = \"40.0\" " +
            "dur = \"13.0\"/>\n" +
            "<set attributename = \"width\" from = \"10.0\" to = \"20.0\" begin = \"30.0\" />\n" +
            "<set attributename = \"height\" from = \"17.0\" to = \"40.0\" begin = \"30.0\" />\n" +
            "</rect>\n" +
            "<ellipse cx=\"40.0\" cy=\"30.0\" rx=\"5.0\" ry=\"10.0\" \n" +
            "visibility = \"hidden\"fill = \"rgb(77,77,51)\">\n" +
            "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\"10.0\"/>\n" +
            "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\"50.0\"/>\n" +
            "</ellipse>\n" +
            "\n" +
            "</svg>\n" +
            "\n";
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 0, 30);
    builder.addMove("R1",40,30,60,60,2,
            10);
    builder.addMove("R1",10,20,200,100,12,
            15);
    builder.addColorChange("R1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,13,14);
    builder.addScaleToChange("R1",10,17,20,40,17,
            30);
    builder.addScaleToChange("O1",10,17,20,40,17,
            30);
    builder.addMove("O1",60,60,100,100,20,
            23);
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,24,27);
    builder.addMove("O1",10,20,200,100,30,
            35);

    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    sVG = new SVGView();
    sVG.initialize(shapes, animations);
    sVG.render(500000, 500000, 1, new PrintStream(out));

    assertEquals(output, out.toString());

  }
}