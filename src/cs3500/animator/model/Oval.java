package cs3500.animator.model;

import java.awt.Color;

/**
 * Oval class. It is a shape.
 */
public class Oval extends AbstractShape {

  private float radiusx;
  private float radiusy;
  private String placement = "Center";


  /**
   * Constructor.
   * @param name        name of the shape.
   * @param color       color.
   * @param position    position.
   * @param radiusx     x radius.
   * @param radiusy     y radius.
   */
  public Oval(String name, Color color, Coordinate position,
      float tAppear, float tDissapear, double tickrate, float radiusx, float radiusy) {
    super(name, color, position, tAppear, tDissapear, tickrate);

    if (radiusx < 0 || radiusy < 0) {
      throw new IllegalArgumentException("Radii for oval must be positive");
    }

    this.radiusx = radiusx;
    this.radiusy = radiusy;

  }

  /**
   * Method to change coordinates of Oval to resize it.
   * @param set New coordinate.
   */
  @Override
  public void setResize(Coordinate set) {
    this.radiusx = set.getXCoord();
    this.radiusy = set.getYCoord();

  }

  /**
   * Get position.
   * @return  position.
   */
  @Override
  public String getPlacement() {
    return this.placement;
  }

  /**
   * Create copy of Oval, to pass to list in View.
   * @return  Oval.
   */
  @Override
  public AbstractShape clone() {
    AbstractShape oval = new Oval(this.name, this.color, this.position,
        this.tAppear, this.tDisappear, this.tickrate, this.radiusx, this.radiusy);
    oval.animations = this.cloneAnimations();
    return oval;
  }

  /**
   * Prints the class name.
   * @return the class name.
   */
  @Override
  public String getClassName() {
    return "Oval";
  }

  /**
   * Prints the dimension of the shape.
   * @return the dimension of the shape.
   */
  @Override
  public float getSpecificDim(String dimtype) {
    if (dimtype.equals("X")) {
      return this.radiusx;

    }
    else if (dimtype.equals("Y")) {
      return this.radiusy;
    }

    else {
      throw new IllegalArgumentException("Entered Invalid Field");
    }
  }

  /**
   * Sets one of the dimensions to the given value.
   * @param dimtype the dimension to be changed
   * @param dim the value the dimension is going to be changed to.
   */
  @Override
  public void setDim(String dimtype, float dim) {
    if (dimtype.equals("X Radius")) {
      this.radiusx = dim;

    }
    else if (dimtype.equals("Y Radius")) {
      this.radiusy = dim;
    }

    else {
      throw new IllegalArgumentException("Entered Invalid Field");
    }
  }

  /**
   * Prints the dimensions of the oval its xradius and yradius.cannersca
   * @return the dimensions in string format
   */
  @Override
  public String printDimensions() {
    String output = "X Radius: " + Float.toString(radiusx) + ", Y Radius: "
        + Float.toString(radiusy);
    return output;
  }

  /**
   * Get width of Oval.
   * @return width.
   */
  @Override
  public float getWidth() {
    return this.radiusx;
  }

  /**
   * Get height of Oval.
   * @return height.
   */
  @Override
  public float getHeight() {
    return this.radiusy;
  }
}
