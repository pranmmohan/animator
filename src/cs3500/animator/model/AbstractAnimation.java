package cs3500.animator.model;

/**
 * Abstract class for animation, since all animations(move, change color, resize) share a certain
 * set of methods.
 */
public abstract class AbstractAnimation {

  protected String shapeID;
  protected int tStart;
  protected int tEnd;
  protected float tickrate;

  /**
   * Constructor for the animation.
   * @param shapeID  ID of the shape.
   * @param start    Start time of animation.
   * @param end      End time of animation.
   */
  AbstractAnimation(String shapeID, int start, int end, int tickrate) {
    this.shapeID = shapeID;
    this.tStart = start;
    this.tEnd = end;
    this.tickrate = tickrate;
  }

  /**
   * Get start time of animation.
   * @return  start time.
   */
  public float getStartTime() {
    return tStart;
  }

  /**
   * Get end time of animation.
   * @return  end time.
   */
  public float getEndTime() {
    return tEnd;
  }

  /**
   * Check if multiple animations of the same kind are happening during the same time.
   * @param a   Animation 1.
   * @param b   Animation 2.
   * @return    true if there is an overlap.
   */
  public static boolean overlappingCommand(AbstractAnimation a, AbstractAnimation b) {
    if (a.getClassName() == b.getClassName() && a.shapeID == b.shapeID) {
      return a.getEndTime() > b.getStartTime() & a.getStartTime() < b.getEndTime();
    } else {
      return false;
    }
  }


  /**
   * toString method to print out the shape ID and animation.
   * @return  description of animation.
   */
  @Override
  public String toString() {
    String output = "Shape " + shapeID + " ";
    output += this.toStringHelper() + " from t=" + Float.toString((float)tStart / tickrate)
        + " to " + "t=" + Float.toString((float)tEnd / tickrate);
    return output;

  }

  /**
   * Get ID of shape.
   * @return  ID of shape.
   */
  public String getID() {
    return this.shapeID;
  }

  /**
   * Method to create a copy of the animation to use in the view.
   * @return  copy of animation.
   */
  public abstract AbstractAnimation clone();

  /**
   * Get the name of the animation.
   * @return  type of animation.
   */
  public abstract String getClassName();

  /**
   * Get initial property of shape (position, color, dimension).
   * @return  property of shape.
   */
  public abstract Object getStart();

  /**
   * Get end property of shape (position, color, dimension).
   * @return  property of shape.
   */
  public abstract Object getEnd();

  /**
   * Print what the animation is and does.
   * @return  action of animation.
   */
  protected abstract String toStringHelper();
}




