package cs3500.animator.model;

import java.util.List;

/**
 * This interface of the Animation model.
 */
public interface Animator {

  /**
   * Add shape to the model.
   * @param s Shape.
   */
  void addShape(AbstractShape s);

  /**
   * Get list of shapes from the model.
   * @return list of shapes.
   */
  List<AbstractShape> getShapes();

  /**
   * Get list of animations from the model.
   * @return list of animations.
   */
  List<AbstractAnimation> getAnimations();

  /**
   * Prints the shape.
   * @param name name of the shape to print.
   * @return the details of the shape.
   */
  String printShape(String name);
}
