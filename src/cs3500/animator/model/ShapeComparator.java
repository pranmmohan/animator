package cs3500.animator.model;

import java.util.Comparator;

/**
 * Class to compare shape times, to create priority queue.
 */
class ShapeComparator implements Comparator<AbstractShape> {

  /**
   * Check if first shape starts later than the next.
   *
   * @param a Shape 1.
   * @param b Shape 2.
   * @return  true if shape 1 appears later than shape 2.
   */
  @Override
  public int compare(AbstractShape a, AbstractShape b) {
    if (a.getTStart() >= b.getTStart()) {
      return 1;
    }
    else {
      return -1;
    }
  }
}