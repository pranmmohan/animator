package cs3500.animator.model;

import cs3500.animator.utils.TweenModelBuilder;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

/**
 * The Model class. It has animation functions.
 */
public final class Model implements Animator {


  private HashMap<String, AbstractShape> nametoShape;
  private PriorityQueue<AbstractAnimation> animations;
  private PriorityQueue<AbstractShape> shapes;
  int ticks;


  /**
   * Constructor for the model class.
   * @param listofshapes list of shapes that this model will work on
   */
  public Model(int tickrate, List<AbstractShape> listofshapes, List<AbstractAnimation> animations) {
    this.shapes = new PriorityQueue<>(new ShapeComparator());
    this.nametoShape = new HashMap<>();
    this.animations = new PriorityQueue<>(new AnimationComparator());
    this.ticks = tickrate;
    for (AbstractShape shape: listofshapes) {
      nametoShape.put(shape.getName(), shape);
      this.shapes.add(shape);
    }
    try {
      for (AbstractAnimation animation : animations) {
        nametoShape.get(animation.shapeID).addAnimation(animation);
        this.animations.add(animation);
      }
    }
    catch (NullPointerException e) {
      throw new IllegalArgumentException("Animation id does not exist");
    }
  }

  /**
   * Adds a ashape to the list of the shapes in the model.
   * @param shape to be added to the model
   */
  public void addShape(AbstractShape shape) {
    this.shapes.add(shape);
    int size = nametoShape.size();
    nametoShape.put(shape.getName(), shape);
    if (nametoShape.size() == size) {
      throw new IllegalArgumentException("You added a shape that already exists, change the id");
    }
  }

  /**
   * Add animations to priority queue of animations.
   * @param abstractAnimation animation.
   */
  public void addAnimation(AbstractAnimation abstractAnimation) {
    this.animations.add(abstractAnimation);
    this.nametoShape.get(abstractAnimation.shapeID).addAnimation(abstractAnimation);
  }

  /**
   * Get list of shapes.
   * @return list of shapes.
   */
  @Override
  public List<AbstractShape> getShapes() {
    List<AbstractShape> toReturn = new ArrayList<>();
    for (AbstractShape shape: shapes) {
      toReturn.add(shape.clone());
    }
    return toReturn;
  }

  /**
   * Get list of animations.
   * @return list of animations.
   */
  @Override
  public List<AbstractAnimation> getAnimations() {
    List<AbstractAnimation> toReturn = new ArrayList<>();
    for (AbstractAnimation animation: animations) {
      toReturn.add(animation.clone());
    }
    return toReturn;
  }

  /**
   * Prints the shape and its animations.
   * @param name name of the shape to print
   * @return details of the shape.
   */
  @Override
  public String printShape(String name) {

    if (nametoShape.get(name) == null) {
      throw new IllegalArgumentException("This shape does not exist");
    }
    String out = "Shapes: \n" + nametoShape.get(name).toString();
    //out += "\n";


    for (AbstractAnimation animation: nametoShape.get(name).animations) {
      out += animation.toString() + "\n";
    }
    return out;
  }


  /**
   * Class to create a model containing a list of shapes and animations.
   */
  public static final class Builder implements TweenModelBuilder<Animator> {

    int tickrate;
    List<AbstractShape> shapes;
    List<AbstractAnimation> animations;

    /**
     * Constructor for Builder.
     * @param tickrate  tickRate which controls time of animation..
     */
    public Builder(int tickrate) {

      this.tickrate = tickrate;
      this.shapes = new ArrayList<>();
      this.animations = new ArrayList<>();
    }

    @Override
    public TweenModelBuilder<Animator> addOval(String name, float cx, float cy, float xRadius,
        float yRadius, float red, float green, float blue, int startOfLife, int endOfLife) {
      AbstractShape oval = new Oval(name, new Color(red, green, blue), new Coordinate(cx, cy),
              startOfLife,
           endOfLife, this.tickrate, xRadius, yRadius);
      shapes.add(oval);
      return this;

    }

    @Override
    public TweenModelBuilder<Animator> addRectangle(String name, float lx, float ly, float width,
        float height, float red, float green, float blue, int startOfLife, int endOfLife) {
      AbstractShape rectangle = new Rectangle(name, new Color(red, green, blue),
          new Coordinate(lx, ly), startOfLife, endOfLife, this.tickrate, width, height);
      shapes.add(rectangle);
      return this;
    }

    @Override
    public TweenModelBuilder<Animator> addMove(String name, float moveFromX, float moveFromY,
        float moveToX, float moveToY, int startTime, int endTime) {
      AbstractAnimation move = new Move(name, startTime, endTime, tickrate,
          new Coordinate(moveFromX, moveFromY), new Coordinate(moveToX, moveToY));
      animations.add(move);
      return this;
    }

    @Override
    public TweenModelBuilder<Animator> addColorChange(String name, float oldR, float oldG,
        float oldB,
        float newR, float newG, float newB, int startTime, int endTime) {
      AbstractAnimation colorChange = new ChangeColor(name, startTime, endTime, tickrate,
          new Color(oldR, oldG, oldB), new Color(newR, newG, newB));
      animations.add(colorChange);
      return this;
    }

    @Override
    public TweenModelBuilder<Animator> addScaleToChange(String name, float fromSx, float fromSy,
        float toSx, float toSy, int startTime, int endTime) {
      AbstractAnimation reSize = new ReSize(name, startTime, endTime, tickrate,
          fromSx, fromSy, toSx, toSy);
      animations.add(reSize);
      return this;
    }

    @Override
    public Animator build() {
      Animator model = new Model(this.tickrate, this.shapes, this.animations);
      return model;
    }
  }
}