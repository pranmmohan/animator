package cs3500.animator.view;


import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.AbstractShape;
import cs3500.animator.model.Coordinate;

import java.awt.Color;
import java.io.IOException;


/**
 * Requirements must add a svg shape string for each shape.
 */
public class SVGView extends AbstractView {


  /**
   * Constructor for SVG view.
   */
  public SVGView() {
    super();
  }


  /**
   * Initialize shape dimensions, tickrate, and appendable.
   *
   * @param width    Width of shape.
   * @param height   height of shape.
   * @param tickrate tickrate.
   * @param ap       appendable.
   */
  @Override
  public void render(int width, int height, int tickrate, Appendable ap) {
    super.width = width;
    super.height = height;
    super.tickrate = tickrate;
    super.ap = ap;
    makeSVG();
  }

  /**
   * Print string of color components.
   *
   * @param c Color.
   * @return color as RGB values.
   */
  private String makeColor(Color c) {
    return "rgb(" + c.getRed() + "," + c.getGreen() + "," + c.getBlue() + ")";
  }

  /**
   * Print animation as a string.
   *
   * @param variable variable of shape being changed.
   * @param from     start value.
   * @param to       end value.
   * @param start    start of animation.
   * @param end      end of animation.
   * @return animation.
   */
  private String makeAnimationString(String variable, String from, String to, float start,
                                     float end) {
    String out = "";
    out += "<animate begin =" + "\"" + start / tickrate + "\"" + " attributename ="
            + "\"" + variable + "\"" + " from = \"" + from + "\" " + "to = \""
            + to + "\"" + " dur = \"" + (end / tickrate
            - start / tickrate) + "\"/ >\n";
    return out;
  }

  /**
   * Set formatter.
   *
   * @param variable variable of shape being changed.
   * @param from     start value.
   * @param to       end value.
   * @param start    start of animation.
   * @return attribute setter.
   */
  private String makeSetterString(String variable, String from, String to, float start) {
    String out = "";
    out += "<set attributename = \"" + variable + "\" from = \"" + from + "\" to = \"" + to + "\" "
            + "begin = \"" + start / tickrate + "\" / >\n";
    return out;
  }


  /**
   * Create animation strings.
   *
   * @param shape shape.
   * @return all the animations.
   */
  private String makeAnimations(AbstractShape shape) {
    String out = "";
    out += "<set attributename=\"visibility\" from=\"hidden\" to=\"visible\" begin=\""
            + shape.getTStart() / tickrate + "\"/>" + "\n";
    out += "<set attributename=\"visibility\" from=\"visible\" to=\"hidden\" begin=\""
            + shape.getTDissapear() / tickrate + "\"/>" + "\n";

    if (shape.getClassName().equals("Rectangle")) {
      for (AbstractAnimation animation : shape.getAnimations()) {
        switch (animation.getClassName()) {

          case "Move":
            Coordinate start = (Coordinate) animation.getStart();
            Coordinate end = (Coordinate) animation.getEnd();
            out += makeAnimationString("x", Float.toString(start.getXCoord()),
                    Float.toString(end.getXCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeAnimationString("y", Float.toString(start.getYCoord()),
                    Float.toString(end.getYCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeSetterString("x", Float.toString(start.getXCoord()),
                    Float.toString(end.getXCoord()), animation.getEndTime());
            out += makeSetterString("y", Float.toString(start.getYCoord()),
                    Float.toString(end.getYCoord()), animation.getEndTime());
            break;
          case "ReSize":
            Coordinate initialdim = (Coordinate) animation.getStart();
            Coordinate enddim = (Coordinate) animation.getEnd();
            out += makeAnimationString("width", Float.toString(initialdim.getXCoord()),
                    Float.toString(enddim.getXCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeAnimationString("height", Float.toString(initialdim.getYCoord()),
                    Float.toString(enddim.getYCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeSetterString("width", Float.toString(initialdim.getXCoord()),
                    Float.toString(enddim.getXCoord()),
                    animation.getEndTime());
            out += makeSetterString("height", Float.toString(initialdim.getYCoord()),
                    Float.toString(enddim.getYCoord()),
                    animation.getEndTime());
            break;

          case "ChangeColor":
            Color startc = (Color) animation.getStart();
            Color endc = (Color) animation.getEnd();
            out += makeAnimationString("fill", makeColor(startc), makeColor(endc),
                    animation.getStartTime(), animation.getEndTime());
            out += makeSetterString("fill", makeColor(startc), makeColor(endc),
                    animation.getEndTime());
            break;

          default:
            throw new IllegalArgumentException("Invalid Animation");

        }
      }
    } else if (shape.getClassName().equals("Oval")) {
      for (AbstractAnimation animation : shape.getAnimations()) {
        switch (shape.getClassName()) {

          case "Move":
            Coordinate start = (Coordinate) animation.getStart();
            Coordinate end = (Coordinate) animation.getEnd();
            out += makeAnimationString("cx", Float.toString(start.getXCoord()),
                    Float.toString(end.getXCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeAnimationString("cy", Float.toString(start.getYCoord()),
                    Float.toString(end.getYCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeSetterString("cx", Float.toString(start.getXCoord()),
                    Float.toString(end.getXCoord()), animation.getEndTime());
            out += makeSetterString("cy", Float.toString(start.getYCoord()),
                    Float.toString(end.getYCoord()), animation.getEndTime());

            break;
          case "ReSize":
            Coordinate initialdim = (Coordinate) animation.getStart();
            Coordinate enddim = (Coordinate) animation.getEnd();
            out += makeAnimationString("rx", Float.toString(initialdim.getXCoord()),
                    Float.toString(enddim.getXCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeAnimationString("ry", Float.toString(initialdim.getYCoord()),
                    Float.toString(enddim.getYCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeSetterString("rx", Float.toString(initialdim.getXCoord()),
                    Float.toString(enddim.getXCoord()),
                    animation.getEndTime());
            out += makeSetterString("ry", Float.toString(initialdim.getYCoord()),
                    Float.toString(enddim.getYCoord()),
                    animation.getEndTime());

            break;

          case "ChangeColor":
            Color startc = (Color) animation.getStart();
            Color endc = (Color) animation.getEnd();
            out += makeAnimationString("fill", makeColor(startc), makeColor(endc),
                    animation.getStartTime(), animation.getEndTime());
            out += makeSetterString("fill", makeColor(startc), makeColor(endc),
                    animation.getEndTime());
            break;

          default:
            throw new IllegalArgumentException("Invalid Animation");

        }

      }
    }
    return out;
  }


  /**
   * Create SVG string of animations.
   */
  private void makeSVG() {
    try {
      String out =
              "<svg width = \"" + width / 100 + "\"" + " height = \"" + height / 100 + "\"" +
                      ">" + "\n\n";

      for (AbstractShape shape : this.shapes) {
        Coordinate coord = shape.getPosition();
        if (shape.getClassName().equals("Rectangle")) {
          out += "<rect x=\"" + coord.getXCoord() + "\" " + "y=\"" + coord.getYCoord() + "\" " +
                  "width=\"" + shape.getWidth() + "\" height =\"" + shape.getHeight() + "\" "
                  + "visibility = \"hidden\"" + " fill = \"" + makeColor(shape.getColor())
                  + "\">\n";
          out += this.makeAnimations(shape);
          out += "</rect>\n";
        } else if (shape.getClassName().equals("Oval")) {
          out += "<ellipse cx=\"" + coord.getXCoord() + "\" cy=\"" + coord.getYCoord() + "\" " +
                  "rx=\"" + shape.getWidth() + "\" ry=\"" + shape.getHeight() + "\" \n"
                  + "visibility = \"hidden\"" + "fill = \"" + makeColor(shape.getColor())
                  + "\">\n";
          out += this.makeAnimations(shape);
          out += "</ellipse>\n";
        }
      }
      out += "\n</svg>";
      out += "\n\n";
      ap.append(out);
    } catch (IOException e) {
      throw new IllegalStateException("Cant append");
    }
  }
}
