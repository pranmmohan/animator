package cs3500.animator.view;

import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.AbstractShape;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Abstract class for each View, as each type of view shares common attributes.
 */
public abstract class AbstractView implements View {


  protected List<AbstractShape> shapes;
  protected List<AbstractAnimation> animations;
  protected HashMap<String, Boolean> appears;
  protected float totalTime;
  protected Appendable ap;
  protected int width;
  protected int height;
  protected int totaltime;
  protected int tickrate;


  /**
   * Constructor initializing shapes list, animation list, hashmap of whether shape appears, and
   * total time of shape appearing.
   */
  public AbstractView() {
    shapes = new ArrayList<>();
    animations = new ArrayList<>();
    appears = new HashMap<>();
    this.totalTime = 0;
    this.width = 0;
    this.height = 0;
    this.totalTime = 0;
    this.tickrate = 0;
  }

  /**
   * Initialize the list of shapes and animations that will be used by the builder to create a
   * model.
   *
   * @param shapes     list of shapes.
   * @param animations list of animations.
   */
  public void initialize(List<AbstractShape> shapes, List<AbstractAnimation> animations) {
    this.shapes = shapes;
    for (AbstractShape shape : shapes) {
      this.totalTime = Float.max(this.totalTime, shape.getTDissapear());
      appears.put(shape.getName(), false);
    }
    this.animations = animations;
  }
}
