package cs3500.animator.utils;


import cs3500.animator.view.SVGView;
import cs3500.animator.view.Textual;
import cs3500.animator.view.View;
import cs3500.animator.view.Visual;

/**
 * Class to create a view based on the input from command line.
 */
public class ViewCreator {


  /**
   * Create view based on input.
   * @param type  type of View.
   * @return  View object(text, visual, or svg).
   */
  public static View create(String type) {
    if (type.equals("text")) {
      return new Textual();
    }
    if (type.equals("svg")) {
      return new SVGView();
    }
    if (type.equals("visual")) {
      return new Visual();
    }
    else {
      throw new IllegalArgumentException("You have entered a view that does not exist");
    }
  }

}
