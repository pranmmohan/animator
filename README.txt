ChangesMade:

Model was completely changed. Initially had an interface where the animator interface had
a method for each of the moves. This was changed so that each move became a class that extended
and AbstractAnimation class that held all of the common fields and common functions held by
the animations, as they share quite a bit. Each shape also includes a list of the animations
that apply to it, sorted by the time the animation starts. The shapes are kept in the model in the
order that they appear. The purpose of the model interface now is to add shapes,
return the list of shapes, list of animations, and print the shape and animation.



View has one interface that has a method for passing the shapes and animations that the view uses,
and another method to start the rendering and display of the rendering according to a specific
tick rate and window size. The window size is only used by the SVG as the visual view takes up
the entire window. The purpose of the view is to accept a list of shapes and animations, and use
the render paramaters to create a view.


To Run:

1) cd into the location of the jar
2) use the command "java -jar Animator.jar ~ ~ ~" (~ refer to the various command line arguments)
	mandatory argument for type of animation : "-iv" which can either be "textual", "svg", or "visual
	mandatory argument for input file : "-if" followed by an a text file which lists the shapes and their animations
3) other arguments
	argument only if type of animation is visual: "-o" followed by a text file where the description of the animation goes
	argument for speed: "-speed" followed by an integer that refers to the tick speed, default is 1

example start command:
"java -jar Animator.jar -if input1.txt -iv visual -speed 20"


